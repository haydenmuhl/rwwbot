from urllib.request import Request
from urllib.parse import quote
import urllib.request
import json
import logging

log = logging.getLogger(__name__)

def toQueryString(map):
  queryString = '&'.join([
      quote(x[0]) + "=" + quote(x[1])
      for x in map.items()
  ])
  return bytes(queryString, "utf-8")

class Reddit:
  
  accessToken = None
  accessTokenUrl = "https://ssl.reddit.com/api/v1/access_token"
  revocationUrl = "https://ssl.reddit.com/api/v1/revoke_token"
  submissionUrl = "https://oauth.reddit.com/api/submit"
  
  def __init__(self, refreshToken, botAuth):
    self.refreshToken = refreshToken
    self.botAuth = botAuth
  
  def hasToken(self):
    return self.accessToken != None
    
  def getToken(self):
    self.accessToken = None
    
    request = self.newAuthRequest(self.accessTokenUrl)
    data = {
        "grant_type": "refresh_token",
        "refresh_token": self.refreshToken
    }
    request.data = toQueryString(data)
    
    response = urllib.request.urlopen(request)
    tokenJson = json.loads(response.read().decode("utf-8"))
    
    self.accessToken = tokenJson["access_token"]
    return True
  
  def revokeToken(self):
    request = self.newAuthRequest(self.revocationUrl)
    data = {
        "token": self.accessToken,
        "token_type_hint": "access_token"
    }
    request.data = toQueryString(data)
    urllib.request.urlopen(request)
    self.accessToken = None
    return True
    
  def newAuthRequest(self, url):
    request = self.newRequest(url)
    request.add_header("Authorization", "Basic " + self.botAuth)
    return request

  def newRequest(self, url):
    request = Request(url)
    request.add_header("User-Agent", "Right Wing Watch bot, by /u/MmmVomit")
    return request
  
  okErrors = ['ALREADY_SUB']
  
  def post(self, title, url):
    request = self.newRequest(self.submissionUrl)
    request.add_header("Authorization", "bearer " + self.accessToken)
    data = {
        "api_type": "json",
        "kind": "link",
        "sr": "rightwingwatch",
        "title": title,
        "url": url,
        "sendreplies": "false",
        "resubmit": "false"
    }
    request.data = toQueryString(data)
    
    response = urllib.request.urlopen(request)
    responseJson = json.loads(response.read().decode("utf-8"))
    errors = responseJson["json"]["errors"]
    if len(errors) > 0:
      for error in errors:
        if error[0] not in self.okErrors:
          raise Exception(errors)
      log.warn("Benign errors: %s\n  %s", errors, url)
    return True

