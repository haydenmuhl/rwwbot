import urllib.request
import xml.etree.ElementTree as etree
import datetime
from os.path import expanduser
import os.path
from reddit import Reddit
import logging

log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(message)s')

format = "%a, %d %b %Y %H:%M:%S %z"

dataDir = "~/.rwwBot/"
timestamp = "timestamp"
refreshToken = "refreshToken"
botAuth = "auth"

def validXmlChar(ch):
  i = ord(ch)

  if i == 0x09:
    return True
  if i == 0x0A:
    return True
  if i == 0x0D:
    return True

  if i < 0x20:
    return False
  if i > 0xD7FF and i < 0xE000:
    return False
  if i > 0xFFFD and i < 0x10000:
    return False

  return True

def cleanXml(xml):
  buffer = []
  for ch in xml.decode("utf-8"):
    if validXmlChar(ch):
      buffer.append(ch)
    else:
      buffer.append(' ')
  return bytes("".join(buffer), "utf-8")

def parseXml(xml):
  xml = cleanXml(xml)
  feed = etree.XML(xml)
  posts = []

  for item in feed.findall("channel/item"):
    pubDate = datetime.datetime.strptime(item.find("pubDate").text, format)
    title = item.find("title").text
    link = item.find("link").text
    posts.append(Post(title, link, pubDate.timestamp()))

  return posts

def getFileContents(path):
  expandedPath = expanduser(path)
  f = open(expandedPath, 'r')
  contents = f.readline().strip()
  f.close()
  return contents

def getTimestamp():
  try:
    ts = int(getFileContents(dataDir + timestamp))
  except (FileNotFoundError, ValueError):
    ts = 0
  return ts

def setTimestamp(lastRun):
  expandedPath = expanduser(dataDir + timestamp)
  f = open(expandedPath, 'w+')
  f.write(str(int(lastRun)))
  f.close()


def filterPosts(posts, timestamp):
  filteredPosts = [x for x in posts if x.timestamp > timestamp]
  filteredPosts.sort(key=lambda p: p.timestamp)
  return filteredPosts

class Post:
  def __init__(self, title, url, timestamp):
    self.title = title
    self.url = url
    self.timestamp = timestamp

if __name__ == "__main__":
  lastRun = getTimestamp()
  auth = getFileContents(dataDir + botAuth)
  token = getFileContents(dataDir + refreshToken)
  log.info("Last run was at %d", lastRun)
  xml = urllib.request.urlopen("http://www.rightwingwatch.org/rss.xml").read()
  posts = parseXml(xml)
  newPosts = filterPosts(posts, lastRun)
  if len(newPosts) > 0:
    rww = Reddit(token, auth)
    try:
      rww.getToken()
      for post in newPosts:
        log.info("Posting (%s)", post.title)
        rww.post(post.title, post.url)
        lastRun = post.timestamp
      rww.revokeToken()
    except urllib.error.HTTPError as e:
      log.exception("Error encountered communicating with Reddit")
  setTimestamp(lastRun)




